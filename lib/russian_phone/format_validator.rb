# coding: utf-8

module RussianPhone
  class FormatValidator < ActiveModel::Validator
    def validate(record)
      options[:fields].each do |field|
        unless record.send(field).phone.blank?
          unless record.send(field).valid? && record.send(field).city_allowed?
            record.errors.add(field, 'Неверный телефонный номер')
          end
        end
      end
    end
  end
end
